// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Registration from '@/components/Registration.vue'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/es5/util/colors'
import VeeValidate from 'vee-validate'

Vue.use(Vuetify, {
  theme: {
    primary: '#A2C938',
    secondary: '#B2FF59',
    accent: '#9CCC65',
    error: colors.red.accent3
  }
})

Vue.use(VeeValidate)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#registration',
  router,
  components: { Registration },
  template: '<Registration/>'
})
